const loggedUser = (context, user) => {
	alert("==========")
	context.commit('LOGGED_USER', user)
}

const logOut = context => {
	context.commit('REMOVE_LOGGED_USER')
}

export default {
	loggedUser,
	logOut
}